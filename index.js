const express = require("express");
const mongoose = require('mongoose'); 
const app = express(); 
const PORT = 5000;

// router
let tasksRoutes = require("./routes/tasksRoutes")

// middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect(`mongodb+srv://aidanbigornia:Big0rnia@cluster0.e3n78.mongodb.net/s31?retryWrites=true&w=majority`,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
).then(() => console.log(`Connected to Database!`))
.catch((error)=>console.log(error));

// schema

// routes
	// request
		// params
		// body
		// header
app.use("/", tasksRoutes);

app.listen(PORT, () => console.log(`Server running on port ${PORT}`));