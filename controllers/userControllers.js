const User = require('./../models/User');

//insert a new user
module.exports.createUser = function(reqBody){
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		userName: reqBody.userName,
		password: reqBody.password
	});

	return newUser.save().then( (savedUser, error) => {
		if(error){
			return error
		} else {
			return savedUser
		}
	});
}

module.exports.getAllUser = () => {
	
	//Model.method
	return User.find().then((result,error)=>{
		if(error){
			return error
		} else {
			//send object/array of object as a response
			return result
		}
	})
	
}

module.exports.getUser = (params) => {

	// Model.method
	return User.findById(params).then((result)=>{
			return result;
		
	});

}

module.exports.updateUser = (params, reqBody) => {

	// let reqBody = req.body
	//Model.method
	return User.findByIdAndUpdate(params, reqBody, {new:true}).then((result,error) =>{
		if(error){
			return error;
		} else {
			// return user object
			return result;
		}

	})
}

module.exports.deleteUser = (params) => {


	return User.findByIdAndDelete(params).then(error =>{
		if(error){
			return error;
		} else {
			// send a response true if deleted
			return true;
		}
	});
}