
let express = require('express');
let router = express.Router();
// model
let User = require('./../models/User');
// controller
let userController = require(`./../controllers/userControllers`)


//insert a new user
router.post("/add-task", (req, res)=>{
	// console.log(req.body);
	/*let newUser = new User({
				firstName: req.body.firstName,
				lastName: req.body.lastName,
				userName: req.body.userName,
				password: req.body.password
			})

	newUser.save( (savedUser, error) => {
		if(error){
			res.send(error)
		} else {
			res.send(`New user saved:`, savedUser);
		}
	})*/

	userController.createUser(req.body).then(result => res.send(result));
});



// retrieve all users
router.get("/users", (req,res)=>{

	// model.method
		// search users from the database
		// our model is User

		// User.find({}, (result, error)=>{
		// 	if(error){
		// 		res.send(error);
		// 	} else {
		// 		res.send(`Users:`,result);
		// 		// return as an array of document
		// 	}

		// })

		// send it as a response

	userController.getAllUser().then(result => res.send(result));

});

// retrieve specific user
router.get("/users/:id", (req,res)=>{
	// console.log(req.params);
	// let params = req.params.id
	// // model.method
	// User.findById(params, (error,result)=>{
	// 	if(error){
	// 		res.send(error)
	// 	} else {
	// 		res.send(result)
	// 	}

	// })

	let params = req.params.id

	userController.getUser(params).then(result => res.send(result));
	// send document as a response
})

// update a user's info
router.put("/users/:id", (req,res)=>{
	/*let params = req.params.id
	User.findByIdAndUpdate(params, req.body, {new:true}, (error,result)=>{
		if(error){
			res.send(error);
		} else {
			res.send(result)
		}
	})
*/
	// req.body
	let params = req.params.id
	userController.updateUser(params, req.body).then(result =>res.send(result));
})

// delete user 
router.delete("/users/:id/", (req,res)=>{
	let params = req.params.id
	//model.method
	// User.findByIdAndDelete(params, (error,result)=>{
	// 	if(error){
	// 		res.send(error)
	// 	} else {
	// 		// send a response true if deleted
	// 		res.send(true);
	// 	}
	// });
	userController.deleteUser(params).then(result =>res.send(result));
});


module.exports = router;
